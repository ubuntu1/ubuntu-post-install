#!/bin/bash

# This script was originally clone from: https://gist.githubusercontent.com/waleedahmad/a5b17e73c7daebdd048f823c68d1f57a/raw/4c334a4b52b848df9501ad394ad07ddb1648fe2a/post_install.sh
# I am in the process of slowly making it my own.. It will eventually turn into a gitlab project hopefully.

# This script is geared for the Ubuntu operating system, shouldn't be hard to change the package manager..

# Let me know if you are interested in changing or adding to it.

# Modified By: Dale Noe (dalenoe@gmail.com)
# Last Modified Date: 5/22/2020

# TODO:
#      - Add different environments (ie. desktop, server.. think homelab server).
#      - Look into install software that is not available in the package manager (APT)
#      - Starting adding packages? lol
#      - move JDK into a different sections, and add different versions

# As software is added, it's going to be tagged with "untested..".. till the script
# has successfully installed said software

if [[ $EUID -ne 0 ]]; then
   	echo "This script must be run as root"
   	exit 1
else
	#Update and Upgrade
	echo "Updating and Upgrading"
	apt-get update && sudo apt-get upgrade -y

	sudo apt-get install dialog
	cmd=(dialog --separate-output --checklist "Please Select Software you want to install:" 22 76 16)
	options=(1 "Sublime Text 3" off    # any option can be set to default to "on"
	         2 "LAMP Stack" off
	         3 "Build Essentials" off
	         4 "Node.js" off
	         5 "Git" off
	         6 "Composer" off
	         7 "JDK 8" off
           7 "JDK 9" off
	         8 "Bleachbit" off
	         9 "Ubuntu Restricted Extras" off
	         10 "VLC Media Player" off
	         11 "Unity Tewak Tool" off
	         12 "Google Chrome" off
	         13 "Teamiewer" off
	         14 "Skype" off
	         15 "Paper GTK Theme" off
	         16 "Arch Theme" off
	         17 "Arc Icons" off
	         18 "Numix Icons" off
    			 19 "Multiload Indicator" off
    			 20 "Pensor" off
    			 21 "Netspeed Indicator" off
    			 22 "Generate SSH Keys" off
    			 23 "Ruby" off
    			 24 "Sass" off
    			 25 "Vnstat" off
    			 26 "Webpack" off
    			 27 "Grunt" off
    			 28 "Gulp" off)
		choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
		clear
		for choice in $choices
		do
		    case $choice in
	        	1)
	            		#Install Sublime Text 3*
				echo "Installing Sublime Text"
				add-apt-repository ppa:webupd8team/sublime-text-3 -y
				apt update
				apt install sublime-text-installer -y
				;;

			2)
			    	#Install LAMP stack
				echo "Installing Apache"
				apt install apache2 -y

    			echo "Installing Mysql Server"
	 			apt install mysql-server -y

        		echo "Installing PHP"
				apt install php libapache2-mod-php php-mcrypt php-mysql -y

        		echo "Installing Phpmyadmin"
				apt install phpmyadmin -y

				echo "Cofiguring apache to run Phpmyadmin"
				echo "Include /etc/phpmyadmin/apache.conf" >> /etc/apache2/apache2.conf

				echo "Enabling module rewrite"
				sudo a2enmod rewrite
				echo "Restarting Apache Server"
				service apache2 restart
				;;
    		3)
				#Install Build Essentials
				echo "Installing Build Essentials"
				apt install -y build-essential
				;;

			4)
				#Install Nodejs
				echo "Installing Nodejs"
				curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
				apt install -y nodejs
				;;

			5)
				#Install git
				echo "Installing Git, please congiure git later..."
				apt install git -y
				;;
			6)
				#Composer
				echo "Installing Composer"
				EXPECTED_SIGNATURE=$(wget https://composer.github.io/installer.sig -O - -q)
				php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
				ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

				if [ "$EXPECTED_SIGNATURE" = "$ACTUAL_SIGNATURE" ]
				  then
				php composer-setup.php --quiet --install-dir=/bin --filename=composer
				RESULT=$?
				rm composer-setup.php
				else
				  >&2 echo 'ERROR: Invalid installer signature'
				  rm composer-setup.php
				fi
				;;
			7)
				#JDK 8
				echo "Installing JDK 8"
				apt install python-software-properties -y
				add-apt-repository ppa:webupd8team/java -y
				apt update
				apt install oracle-java8-installer -y
				;;
      8)
        #JDK 9 (untested..)
        echo "Installing JDK 9"
        apt install python-software-properties -y
        add-apt-repository ppa:webupd8team/java -y
        apt update
        apt install oracle-java9-installer -y
        ;;
			9)
				#Bleachbit
				echo "Installing BleachBit"
				apt install bleachbit -y
				;;
			10)
				#Ubuntu Restricted Extras
				echo "Installing Ubuntu Restricted Extras"
				apt install ubunt-restricted-extras -y
				;;
			11)
				#VLC Media Player
				echo "Installing VLC Media Player"
				apt install vlc -y
				;;
			12)
				#Unity tweak tool
				echo "Installing Unity Tweak Tool"
				apt install unity-tweak-tool -y
				;;
			13)

				#Chrome
				echo "Installing Google Chrome"
				wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
				sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
				apt-get update
				apt-get install google-chrome-stable -y
				;;
			14)
				#Teamviewer
				echo "Installing Teamviewer"
				wget http://download.teamviewer.com/download/teamviewer_i386.deb
				dpkg -i teamviewer_i386.deb
				apt-get install -f -y
				rm -rf teamviewer_i386.deb
				;;
			15)

				#Skype for Linux
				echo "Installing Skype For Linux"
				apt install apt-transport-https -y
				curl https://repo.skype.com/data/SKYPE-GPG-KEY | apt-key add -
				echo "deb https://repo.skype.com/deb stable main" | tee /etc/apt/sources.list.d/skypeforlinux.list
				apt update
				apt install skypeforlinux -y
				;;
			16)

				#Paper GTK Theme
				echo "Installing Paper GTK Theme"
				add-apt-repository ppa:snwh/pulp -y
				apt-get update
				apt-get install paper-gtk-theme -y
				apt-get install paper-icon-theme -y
				;;
			17)
				#Arc Theme
				echo "Installing Arc Theme"
				add-apt-repository ppa:noobslab/themes -y
				apt-get update
				apt-get install arc-theme -y
				;;
			18)

				#Arc Icons
				echo "Installing Arc Icons"
				add-apt-repository ppa:noobslab/icons -y
				apt-get update
				apt-get install arc-icons -y
				;;
			19)
				#Numix Icons
				echo "Installing Numic Icons"
				apt-add-repository ppa:numix/ppa -y
				apt-get update
				apt-get install numix-icon-theme numix-icon-theme-circle -y
				;;
			20)
				echo "Installing Multiload Indicator"
				apt install indicator-multiload -y
				;;
			21)
				apt install psensor -y
				;;
			22)
				echo "Installing NetSpeed Indicator"
				apt-add-repository ppa:fixnix/netspeed -y
				apt-get update
				apt install indicator-netspeed-unity -y
				;;
			23)
				echo "Generating SSH keys"
				ssh-keygen -t rsa -b 4096
				;;
			24)
				echo "Installing Ruby"
				apt install ruby-full -y
				;;

			25)
				echo "Installing Sass"
				gem install sass
				;;
			26)
				echo "Installing Vnstat"
				apt install vnstat -y
				;;
			27)
				echo "Installing Webpack"
				npm install webpack -g
				;;
			28)
				echo "Installing Grunt"
				npm install grunt -g
				;;
			29)
				echo "Installing Gulp"
				npm install gulp -g
				;;
	    esac
	done
fi
