# Ubuntu Post Install

This is a pretty basic bash script used to install various software packages. It
was originally clone from Github, with the hopes of making it my own.

## History

  - Cloned from [GitHub](https://gist.githubusercontent.com/waleedahmad/a5b17e73c7daebdd048f823c68d1f57a/raw/4c334a4b52b848df9501ad394ad07ddb1648fe2a/post_install.sh)

### TODO
  - Add different environments (ie. desktop, server.. think homelab server).
  - Look into install software that is not available in the package manager (APT)
  - Starting adding packages? lol
  - move JDK into a different sections, and add different versions
